\documentclass[aspectratio=169]{beamer}
\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing, positioning}
\newcommand{\tikzmark}[3][]{\tikz[remember picture,baseline] \node[inner sep=0pt] [anchor=base,#1](#2) {#3};}
\usepackage{caption}
% \usetheme{metropolis}
\setbeamertemplate{footline}[frame number]
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{bbold}
\usepackage[style=authoryear]{biblatex}
\usepackage{xpatch}
\xapptobibmacro{cite}{\setunit{\nametitledelim}\printfield{title}}{}{}  % To make the format `AUTHOR+YEAR+TITLE`. \setunit makes sure spacing is ok
\addbibresource{bibliography.bib}
\renewcommand{\footnotesize}{\fontsize{5pt}{5pt}\selectfont}  % To make footnotes fit
% \newcommand{\myfootcite}[1]{\cite{#1} \citeauthor{#1} \citeyear{#1}, \citetitle{#1}}
\newcommand{\myfootcite}[1]{\cite{#1}}
% \setbeamertemplate{frametitle continuation}{[\insertcontinuationcount]}
\AtBeginBibliography{\tiny}


% \usepackage{footmisc, bigfoot}
% \def\footnotelayout{\color{darkgray}}
\title{
  Adressing safety concerns for RL based Navigation\\and Planning using Uncertainty Estimation
}
\subtitle{
  Seminar for Robotics for CSE, Fall 2020
}
\author{
  Romeo Valentin\\romeo.valentin@math.ethz.ch
}
\date{December 19, 2020}

\begin{document}
\captionsetup{belowskip=0pt, aboveskip=0pt}
% \setlength{\intextsep}{0pt}
% \setlength{\dbltextfloatsep}{0pt}

\frame{\titlepage}

\begin{frame} % Safety definition
  \centering
  {\usebeamercolor[fg]{frametitle}\huge \textit{Safety in our context}}
  % \usebeamercolor[fg]{frametitle}{\huge \textit{Safety in our context}}

  \vspace{1cm}
  \begin{columns}[t]
    \begin{column}{0.65\textwidth}
      \begin{itemize}
        \item Model is given, but stochastic or uncertain
        \item Probability of avoiding unrecoverable state
        \item Model robustness to unseen inputs
        \item Enforced constraints for RL
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame} % Topic overview
  \begin{columns}[t]
    \begin{column}{0.33\textwidth}
      \begin{block}{\tikzmark{model-based-rl-1}{Model based RL}}
        \begin{itemize}
          \item MDPs\footnotemark[1] + GPs\footnotemark[2]
          \item Approximation by \tikzmark{model-based-rl-end-1}{NN}\footnotemark[3]
        \end{itemize}
      \end{block}
    \end{column}
    \begin{column}{0.33\textwidth}
      \begin{block}{Model free RL}
        \begin{itemize}
          \item PPO/TRPO
          % \item[] vs.
          \item CPO\tikzmark{cpo}{}
        \end{itemize}
      \end{block}
    \end{column}
    \begin{column}{0.33\textwidth}
      \begin{block}{Other Algos}
        \begin{itemize}
          \item Object detection / CV
          \item Imitation learning\tikzmark{planning}{}
        \end{itemize}
      \end{block}
    \end{column}
  \end{columns}
  \begin{tikzpicture}[overlay, remember picture]
    \only<2>{ 
      \draw[thick,red, rounded corners] (model-based-rl-1.north west) ++(-3pt,3pt) rectangle ([xshift=5pt,yshift=-3pt]model-based-rl-end-1.south east); 
      \node[red] [above=3pt of model-based-rl-1.north west, anchor=south west] {Now};
    }
  \end{tikzpicture}
  % \begin{tikzpicture}[overlay, remember picture]
  %   \draw[decorate, decoration={brace, mirror}] (cpo.south west) -- (planning |- cpo.south) coordinate[midway] (brace-mid);
  %   \node at (brace-mid) [below] {uncertainty estimation};
  % \end{tikzpicture}

  % \vspace{t}
  \begin{columns}[t]
    \begin{column}{0.33\textwidth}
      \begin{figure}[t]
        \caption*{\tiny \textbf{Safe MDP traversal}\footnotemark[1]}
        % \includegraphics[width=\textwidth]{figs/GP-UCB.png}
        \includegraphics[width=0.7\textwidth]{figs/sink-mdp.png}
      \end{figure}
    \end{column}
    \begin{column}{0.33\textwidth}
      \begin{figure}
        \caption*{\tiny \textbf{Uncertainty estimation}\footnotemark[4]}
        \includegraphics[width=\textwidth]{figs/GP-Regression.png}
      \end{figure}
    \end{column}
    \begin{column}{0.33\textwidth}
      \begin{figure}
        \caption*{\tiny \textbf{Uncertainty for Dense Segmentation}\footnotemark[5]}
        \includegraphics[width=\textwidth]{figs/Dense-Segmentation.png}
      \end{figure}
    \end{column}
  \end{columns}
  \footnotetext[1]{%Moldovan et. al, 2012
    %\cite{Moldovan12}
    \myfootcite{Moldovan12}}
    \footnotetext[2]{\myfootcite{Turchetta16}}
    \footnotetext[3]{\myfootcite{Chua18}}
    \footnotetext[4]{Krause 2020, Probabilistic Artificial Intelligence Lecture (ETH)}
  \footnotetext[5]{%Kendall et. al, 2018
    \myfootcite{Kendall17}}

  % \vspace{10pt}
  \begin{block}{$\Rightarrow$ Uncertainty estimation and out-of-distribution detection}\end{block}
\end{frame}

\begin{frame} \frametitle{Uncertainty estimation}
  \framesubtitle{For now: Gaussian Processes and MDPs}

  Given prior belief and measurements, we have posterior belief about model
  \begin{itemize}
    \item \tikzmark{uncertainty-about-model}{Uncertainty about model {\tiny(due to lacking data)}}
    \item \tikzmark{inherent-model-stochasticity}{Inherent model stochasticity }  
  \end{itemize}
  \begin{tikzpicture}[overlay, remember picture, shorten >= 3pt]
    % \coordinate (aleatoric-epistemic-x-pos) at [above=2cm of uncertainty-about-model.east];
    \node (aleatoric-annotation) [right = 1cm of uncertainty-about-model.east, anchor=west] {\tiny \textcolor{darkgray}{Epistemic uncertainty}};
    \draw[->, darkgray] (aleatoric-annotation) to (uncertainty-about-model);
    \node (epistemic-annotation) at (aleatoric-annotation.west |- inherent-model-stochasticity) [anchor=west] {\tiny\textcolor{darkgray}{Aleatoric uncertainty}};
    \draw[->, darkgray] (epistemic-annotation) to (inherent-model-stochasticity -| uncertainty-about-model.east);
  \end{tikzpicture}
  \vspace{-10pt}
  % \only<1-2>{
  %   \begin{figure}
  %     \centering
  %       \caption*{\tiny \textbf{MDP example\footnotemark[1]}}
  %       \includegraphics[height=2cm]{figs/sink-mdp.png}
  %   \end{figure}
  % }
  % \only<2>{
  %   \[ \begin{aligned}
  %     &\text{given } s_0, \pi_{\mathit{out}} \rightarrow s_t \\
  %      &\exists \pi_{\mathit{ret}} \ \mathit{ s.t. } \ 
  %        \mathbb{E}_{s_t, \pi_{\mathit{ret}}} [\mathbb{1}_{\mathit{return}}]\geq \delta
  %     \end{aligned}
  %   \]
  % }
  % \only<1-2>{
  %   \footnotetext[1]{Moldovan and Abeel, 2012}
  % }
    \begin{columns}[T]
      \begin{column}{0.5\textwidth}
        \centering
        \begin{figure}
          \centering
          \caption*{\tiny \textbf{MDP example\footnotemark[1]}}
          \includegraphics[height=2cm]{figs/sink-mdp.png}
        \end{figure}
        \uncover<2->{
        \[ \begin{aligned}
          &\text{given } s_0, \pi_{\mathit{out}} \rightarrow s_t \\
           &\exists \pi_{\mathit{ret}} \ \mathit{ s.t. } \ 
             \mathbb{E}_{s_t, \pi_{\mathit{ret}}} [\mathbb{1}_{\mathit{return}}]\geq \delta
          \end{aligned}
        \]
        }
      \end{column}
      \begin{column}{0.5\textwidth}
        \uncover<3->{
        \begin{figure}
          \centering
          \caption*{\tiny \textbf{Gaussian Process\footnotemark[2]}}
          \includegraphics[height=3cm]{figs/GP-Regression.png}
        \end{figure}
        }
      \end{column}
    \end{columns}
    \footnotetext[1]{%Moldovan and Abeel, 2012
      \myfootcite{Moldovan12}}
      \footnotetext[2]{Krause 2020, Probabilistic Artificial Intelligence Lecture (ETH)}
\end{frame}

\begin{frame} \frametitle{Uncertainty estimation}
  \framesubtitle{Extension from GPs: Neural Networks and Model Based RL (MBRL)}

  Now: MDP $\rightarrow$ Fully learned system dynamics {\tiny(MBRL)}

  \only<1-2>{
  \begin{figure}[t]
    \caption*{\tiny \textbf{Model Based RL with Uncertainty}\footnotemark[1]}
    % \includegraphics[height=2cm]{figs/deep-rl-uncertainty.png}
    % \includegraphics[height=2cm]{figs/deep-rl-uncertainty2.png}
    \includegraphics[height=2cm]{figs/deep-rl-uncertainty-1.png}
  \end{figure}
  \footnotetext[1]{%Chua et. al, 2018
    \myfootcite{Chua18}}
  }
  \only<3->{
    \footnotetext[1]{%Chua et. al, 2018
      \myfootcite{Chua18}}
    \begin{figure}[t]
      \centering
      \caption*{\tiny \textbf{Model Based RL with Uncertainty}\footnotemark[1]}
      % \includegraphics[height=2cm]{figs/deep-rl-uncertainty.png}
      % \includegraphics[height=2cm]{figs/deep-rl-uncertainty2.png}
      \includegraphics[height=2cm]{figs/deep-rl-uncertainty-full.png}
    \end{figure}
    
  }

  \uncover<2->{
    \begin{block}{How to compute uncertainty for NNs?}
      \begin{itemize}
        \item Bootstrapped ensembles
        \item Bayesian Neural Networks
        \item Dropout
      \end{itemize}
    \end{block}
  }
\end{frame}

\begin{frame} \frametitle{Uncertainty estimation}
  \framesubtitle{Using uncertainty estimation for efficient exploration}
  \begin{columns}[T]
    \begin{column}{0.3\textwidth}
      \begin{figure}
        \centering
        \caption*{\tiny \textbf{Exploration vs exploitation in GPs\footnotemark[1]}}
        \includegraphics[width=1\textwidth]{figs/GP-UCB.png}
      \end{figure}
    \end{column}
    \begin{column}{0.7\textwidth}
    \begin{figure}
      \centering
      \caption*{\tiny \textbf{Exploration vs exploitation for MBRL\footnotemark[2]}}
        \includegraphics[width=1\textwidth]{figs/deep-rl-ucb.png}
      \end{figure}
    \end{column}
  \end{columns}
  \footnotetext[1]{Krause 2020, Probabilistic Artificial Intelligence Lecture (ETH)}
  \footnotetext[2]{%Curi et. al, 2020
    \myfootcite{Curi20}}

  \vspace{10pt}
  Pick $x_t$ that maximizes upper confidence bound:
  \[
    x_{t+1} = \underset{x \in D}{\mathit{argmax}}\ \tikzmark{mu}{$\mu_t$}(x) + \tikzmark{beta}{$\beta$} \tikzmark{sigma}{$\sigma_t$}(x)
  \]
  \begin{tikzpicture}[overlay, remember picture, node distance=25pt, shorten >= 2pt]
    \node[align=center,font=\fontsize{5pt}{5pt}\selectfont,inner sep=2pt] (lab1coord) [below of=mu] { approx.\\ function\\ mean};
    \draw[->] (lab1coord) to [in=-90,out=90] (mu);

    \node[align=center,font=\fontsize{5pt}{5pt}\selectfont,inner sep=2pt] (lab2coord) [below of=sigma] { approx.\\ function\\ uncertainty};
    \draw[->] (lab2coord) to [in=-90,out=90] (sigma);

    \node[align=center,font=\fontsize{5pt}{5pt}\selectfont,inner sep=2pt] (lab3coord) [above right=7pt and 15pt of beta] { trade off constant};
    \draw[->] (lab3coord) to [in=90,out=180] (beta);
  \end{tikzpicture}
\end{frame}

\begin{frame}
  \begin{columns}[t]
    \begin{column}{0.33\textwidth}
        \begin{block}{\tikzmark{model-based-rl}{Model based RL}}
          \begin{itemize}
            \item MDPs + GPs
            \item Approximation by \tikzmark{model-based-rl-end}{NN}
          \end{itemize}
        \end{block}
    \end{column}
    \begin{column}{0.33\textwidth}
      \begin{block}{\tikzmark{model-free-rl}{Model free RL}}
        \begin{itemize}
          \item PPO\footnotemark[1]/\tikzmark{trpo}{TRPO}\footnotemark[2]
          % \item[] vs.
          \item CPO\footnotemark[3]\tikzmark{cpo}{}
        \end{itemize}
      \end{block}
    \end{column}
    \begin{column}{0.33\textwidth}
      \begin{block}{\tikzmark{other-algos}{Other Algos}}
        \begin{itemize}
          \item Object detection / \tikzmark{cv}{CV}\footnotemark[4]
          \item \tikzmark{planning}{Imitation learning}\footnotemark[5]
        \end{itemize}
      \end{block}
    \end{column}
  \end{columns}
  \begin{tikzpicture}[overlay, remember picture]
    \only<1>{ \draw[thick,red, rounded corners] (model-based-rl.north west) ++(-3pt,3pt) rectangle ([xshift=3pt,yshift=-3pt]model-based-rl-end.south east); 
      \node[red] [above=3pt of model-based-rl.north west, anchor=south west] {So far};
    }
    \only<2>{ \draw[thick,red, rounded corners] (model-free-rl.north west) ++(-3pt,3pt) rectangle ([xshift=5pt,yshift=-3pt]cpo.south -| trpo.east); 
      \node[red] [above=3pt of model-free-rl.north west, anchor=south west] {Out of scope};
    }
    \only<3>{ \draw[thick,red, rounded corners] (other-algos.north west) ++(-3pt,3pt) rectangle ([xshift=5pt,yshift=-3pt]planning.south -| cv.east); 
      \node[red] [above=3pt of other-algos.north west, anchor=south west] {Next up};
    }
  \end{tikzpicture}

  % \vspace{t}
  \begin{columns}[t]
    \begin{column}{0.33\textwidth}
      \begin{figure}[t]
        \caption*{\tiny \textbf{Safe MDP traversal}}
        % \includegraphics[width=\textwidth]{figs/GP-UCB.png}
        \includegraphics[width=0.7\textwidth]{figs/sink-mdp.png}
      \end{figure}
    \end{column}
    \begin{column}{0.33\textwidth}
      \begin{figure}
        \caption*{\tiny \textbf{Uncertainty estimation}}
        \includegraphics[width=\textwidth]{figs/GP-Regression.png}
      \end{figure}
    \end{column}
    \begin{column}{0.33\textwidth}
      \begin{figure}
        \caption*{\tiny \textbf{Uncertainty for Dense Segmentation}\footnotemark[4]}
        \includegraphics[width=\textwidth]{figs/Dense-Segmentation.png}
      \end{figure}
    \end{column}
  \end{columns}
  \footnotetext[1]{\myfootcite{PPO}}
  \footnotetext[2]{\myfootcite{TRPO}}
  \footnotetext[3]{\myfootcite{CPO}}
  \footnotetext[4]{\myfootcite{Kendall17}}
  \footnotetext[5]{\myfootcite{ImitationLearning}}

  \begin{block}{$\Rightarrow$ Uncertainty estimation and out-of-distribution detection}\end{block}
\end{frame}

\begin{frame} \frametitle{Uncertainty for Semantic Segmentation and Out-of-distribution Detection}
  \framesubtitle{Application example}

  \begin{columns}[T]
    \begin{column}{0.70\textwidth}
      \begin{figure}
        \caption*{\tiny \textbf{Uncertainty for Dense Segmentation}\footnotemark[1]}
        \includegraphics[width=0.8\textwidth]{figs/Dense-Segmentation.png}
      \end{figure}
      \vspace{-10pt}
      \begin{itemize}
        \item Dropout as approximation to BNN
        \item Explicit modeling of aleatoric uncertainty
      \end{itemize}
    \end{column}
    \begin{column}{0.30\textwidth}
      \uncover<2->{
        \begin{figure}
          \caption*{\tiny \textbf{Out-of-distribution detection with Anymal}\footnotemark[2]}
          \includegraphics[height=2cm]{figs/anomaly-examples-anymal.png}
          \includegraphics[width=\textwidth]{figs/anomaly-examples.png}
        \end{figure}
      }
    \end{column}
  \end{columns}
  \footnotetext[1]{%Kendall et. al, 2017
    \myfootcite{Kendall17}}
  \footnotetext[2]{%Wellhausen et. al, 2020
    \myfootcite{Wellhausen20}}
\end{frame}

\begin{frame} \frametitle{Out-of-distribution detection using Autoencoder}
  \framesubtitle{Application example}
  \begin{columns}
    \begin{column}{0.3\textwidth}
      \begin{block}{Autoencoder approach}
        \begin{itemize}
          \item Train AE on identity mappings on seen situations
          \item At eval time, check if reconstruction performs well
        \end{itemize}
      \end{block}
    \end{column}
    \begin{column}{0.7\textwidth}
      \begin{figure}
        \centering
        \caption*{Task aware uncertainty using AE\footnotemark[1]}
        \rule{0.6\textwidth}{0.5pt}
        \begin{tikzpicture}[>=stealth]
          \node[anchor=south west, inner sep=0] (image1) at (0,0) {
            \includegraphics[width=0.8\textwidth]{figs/autoencoder-train.png}
          };
          \begin{scope}[x={(image1.south east)}, y={(image1.north west)}]
            \coordinate (out1) at[anchor=center] (0., 0.7);
            \node (AE1) at (-0.05, 0.45) {AE};
            \coordinate (in1) at (0.,0.2);
            \draw[->,thick] (out1) to[out=180,in=90] (AE1);
            \draw[->,thick] (AE1) to[out=-90,in=180] (in1);
          \end{scope}
        \end{tikzpicture}
        \rule{0.6\textwidth}{0.5pt}
        \begin{tikzpicture}[>=stealth]
          \node[anchor=south west, inner sep=0] (image1) at (0,0) {
            \includegraphics[width=0.8\textwidth]{figs/autoencoder-test.png}
          };
          \begin{scope}[x={(image1.south east)}, y={(image1.north west)}]
            \coordinate (out1) at[anchor=center] (0., 0.7);
            \node (AE1) at (-0.05, 0.45) {AE};
            \coordinate (in1) at (0.,0.2);
            \draw[->,thick] (out1) to[out=180,in=90] (AE1);
            \draw[->,thick] (AE1) to[out=-90,in=180] (in1);
          \end{scope}
        \end{tikzpicture}
      \end{figure}
    \end{column}
  \end{columns}
  \footnotetext[1]{%McAllister et. al, 2019
    \myfootcite{McAllister19}}
\end{frame}

\begin{frame}
  \frametitle{Final overview}
  % \framesubtitle{}
  % \begin{itemize}
  %   \item Recoverability under uncertain dynamics
  %   \item Robustness to out-of-distribution / confidence estimation
  % \end{itemize}

  \centering
  \begin{tikzpicture}[shorten >= 2pt, >=stealth]
    \node[draw, minimum height=0.70cm] (MDP) {MDP\footcite{Moldovan12}};
    \node[draw, minimum height=0.70cm] (GP) [right =1.5cm of MDP] {GP};
    \node[right=0.6cm of GP, scale=0.6, align=center] (much-literature) {many more\\methods};
    \draw[->, dashed] (GP) to[out=0, in=-180] (much-literature);
    \draw[->] (GP) to node[midway, above, text width=1.25cm, align=center, scale=0.5] {Uncert. approx.} (MDP);
    \node[draw, minimum height=0.70cm] (MBRL) [below =of MDP] {Deep RL\footcite{Chua18}};
    \draw[->, dashed] (MDP) to (MBRL);
    \node[draw, minimum height=0.70cm] (BNN) [below =of GP] {BNN};
    \draw[->, dashed] (GP) to (BNN);
    \draw[->] (BNN) to node[midway, above, text width=1.25cm, align=center, scale=0.5] {Uncert. approx.} (MBRL);
    \node[draw, minimum height=0.70cm] (sem-seg) [right =of BNN] {Sem. Seg.\footcite{Kendall17}};
    \draw[->] (BNN) to (sem-seg);
    \node[draw, minimum height=0.70cm] (ood) [below =of BNN] {Out of distr. detection};
    \draw[->] (MBRL) to (ood);
    \draw[->] (sem-seg) to (ood);
    \node[draw, minimum height=0.70cm] (AE) [right =of ood] {Autoencoder\footcite{McAllister19}};
    \draw[->] (AE) to (ood);

    \node[draw, left=of MDP] (mdp) {\includegraphics[width=2.5cm]{figs/sink-mdp.png}};
    \draw[gray, shorten >=-0pt] (MDP) to (mdp);
    \node[draw, below left=0.2cm and 0.5cm of MBRL] (drl) {\includegraphics[width=2.5cm]{figs/deep-rl-uncertainty-1.png}};
    \draw[gray, shorten >=-0pt] (MBRL) to (drl);
    \node[draw, above right=0.2cm and 0.5cm of sem-seg] (seg) {\includegraphics[width=2.5cm]{figs/Dense-Segmentation.png}};
    \draw[gray, shorten >=-0pt] (sem-seg) to (seg);
    \node[draw, below right=0.5cm and -0.9cm of AE] (ae) {\includegraphics[width=3.0cm]{figs/Autoencoder-Test.png}};
    \draw[gray, shorten >=-0pt] (AE) to (ae);
    \node[draw, below=0.7cm of ood] (ood2) {\includegraphics[width=2.0cm]{figs/anomaly-examples-anymal.png}};
    \draw[gray, shorten >=-0pt] (ood) to (ood2);
    
    
  \end{tikzpicture}
  
\end{frame}

\begin{frame}
  \frametitle{Further papers considered}
  \framesubtitle{Not covered or extentions on presented topics}
  \setcounter{footnote}{0}
  \begin{itemize}
    \item Constrained policy optimization \footcite{CPO}
    \item Imitation learning from experts \footcite{ImitationLearning}$^,$\footcite{Duan17}
    \item Neural Network Calibration \footcite{Guo17}
    \item Lyapunov function approach for safe Model Based RL \footcite{Berkenkamp17}$^,$\footcite{Fisac18}
    \item Sim-to-real RL case study\footcite{Lee20}
    \item Gaussian Processes for safe Model Based RL \footcite{Koller18}$^,$\footcite{Berkenkamp16}
  \end{itemize}
  
\end{frame}

\begin{frame}[allowframebreaks]
    \frametitle{References}
    % \nocite{*}
    \printbibliography
    % \bibliographystyle{amsalpha}
    % \bibliography{bibliography.bib}
\end{frame}
\end{document}
